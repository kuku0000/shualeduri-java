import com.example.shualedurijava.Shope;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.persistence.*;

public class SimpleTest {

    @Test
    void test_db_writing() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        EntityTransaction entityTransaction = entityManager.getTransaction();

        try {
            entityTransaction.begin();

            Shope b  = new Shope();
            b.setName("uykl");
            b.setBrand("Intel");
            b.setPrice(2000);
            b.setNames("Cpu");


            entityManager.persist(b);

            entityTransaction.commit();

            TypedQuery<Shope> test_query = entityManager.createQuery(
                    "SELECT b FROM Shope b WHERE b.names = :names", Shope.class
            );
            Shope result = test_query.setParameter("names", "Cpu")
                    .getSingleResult();

            assertNotNull(result);

        } finally {
            if (entityTransaction.isActive()) {
                entityTransaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}

