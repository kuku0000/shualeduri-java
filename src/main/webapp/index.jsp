<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Hello World!" %>
</h1>
<br/>

<form action="data.jsp" method="post" target="_blank">
    Name: <input type="text" name="shope_name">
    <br>
    Brand: <input type="text" name="shope_brand">
    <br>
    Price: <input type="text" name="shope_price">
    <br>
    Names: <input type="text" name="shope_names">
    <br>
    <input type="submit" value="Check">
</form>

<% if(request.getParameter("is_successful") != null &&
        request.getParameter("is_successful").equals("yes")) { %>
<p>Information written into DB successfully!</p>
<% } %>

</body>
</html>