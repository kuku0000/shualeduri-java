<%@ page import="javax.persistence.EntityManagerFactory" %>
<%@ page import="javax.persistence.Persistence" %>
<%@ page import="javax.persistence.EntityManager" %>
<%@ page import="javax.persistence.EntityTransaction" %>
<%@ page import="com.example.shualedurijava.Shope" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<% String isSuccessful = "no"; %>

<%

    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");

    EntityManager entityManager = entityManagerFactory.createEntityManager();

    EntityTransaction entityTransaction = entityManager.getTransaction();

    try {
        entityTransaction.begin();

        Shope b  = new Shope();
        b.setName(request.getParameter("shope_name"));
        b.setBrand(request.getParameter("shope_brand"));
        b.setPrice(Integer.parseInt(request.getParameter("shope_price")));
        b.setNames(request.getParameter("shope_names"));


        entityManager.persist(b);

        entityTransaction.commit();

        isSuccessful = "yes";

    } finally {
        if (entityTransaction.isActive()) {
            entityTransaction.rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
    }

%>

<form id="finalForm" action="index.jsp" method="post">
    <input type="hidden" name="is_successful" value="<%= isSuccessful %>">
</form>

<script>
    document.getElementById("finalForm").submit();
</script>
</body>
</html>
