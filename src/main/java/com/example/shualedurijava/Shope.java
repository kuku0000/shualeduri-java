package com.example.shualedurijava;
import javax.persistence.*;
@Entity
@Table(name = "SHOPE")
public class Shope {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "BRAND")
    private String brand;

    @Column(name = "PRICE")
    private int price;

    @Column(name = "NAMES")
    private String names;



    public Shope() {
    }
    public Shope(long id, String name, String brand, int price, String names) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.names = names;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }






    @Override
    public String toString() {
        return "Shope{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                ", name2='" + names + '\'' +
                '}';
    }

}
